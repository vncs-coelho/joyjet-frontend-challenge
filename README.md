# Joyjet Front-end Challenge

This is a quiz app made as a technical challenge for the front-end developer position @ Joyjet.

## Project setup

```
npm install
```

### Compiles and serves to localhost:8080

```
npm run serve
```

### Try online

http://hardcore-austin-2711ed.netlify.com
