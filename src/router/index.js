import Vue from "vue"
import VueRouter from "vue-router"
import QuizzesList from "../views/QuizzesList.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "quizzes.list",
    component: QuizzesList
  },
  {
    path: "/new",
    name: "quizzes.new",
    component: () => import("../views/QuizzesForm.vue")
  },
  {
    path: "/quiz/:id/:title",
    name: "quizzes.view",
    component: () => import("../views/QuizzesView.vue")
  }
]

const router = new VueRouter({
  routes
})

export default router
