import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
/* Tailwind */
import "@/assets/css/tailwind.css"

Vue.config.productionTip = false

/* Slugify - https://gist.github.com/mathewbyrne/1280286 */
Vue.mixin({
  methods: {
    slugify(text) {
      return text
        .toString()
        .toLowerCase()
        .replace(/\s+/g, "-")
        .replace(/[^\w-]+/g, "")
        .replace(/--+/g, "-")
        .replace(/^-+/, "")
        .replace(/-+$/, "")
    }
  }
})

/* Hydrate the store with localStorage data */
if (localStorage.persistentData) {
  store.replaceState(JSON.parse(localStorage.persistentData))
}

/* Persist data to localStorage every time a mutation is triggered */
store.subscribe((_, store) => {
  localStorage.persistentData = JSON.stringify(store)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app")
