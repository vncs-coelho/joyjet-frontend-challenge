const state = {
  awnseredQuizzes: [
    /* {
      quiz_id: 0,
      awnsers: [
        {
          question_id: 0,
          awnser: 1
        }
      ]
    } */
  ]
}

const getters = {
  getAwnsers: state => (quizId, questionId) => {
    const currentQuestion = state.awnseredQuizzes.find(
      quiz =>
        quiz.quiz_id === quizId &&
        quiz.awnsers.find(awnser => awnser.question_id === questionId)
    )

    if (currentQuestion) return currentQuestion.awnsers[questionId].awnser

    return null
  },
  getScore: state => quizId => {
    const currentQuiz = state.awnseredQuizzes.find(({ id }) => id === quizId)

    if (!currentQuiz) return 0

    return currentQuiz.awnsers.filter(
      ({ awnser, correctAwnser }) => awnser === correctAwnser
    ).length
  }
}

const actions = {
  saveAwnser({ commit, state }, { quizId, questionId, awnser, correctAwnser }) {
    const quizIndex = state.awnseredQuizzes.findIndex(
      quiz => quiz.quiz_id === quizId
    )

    if (quizIndex === -1) {
      commit("addQuizAndAwnsers", { quizId, questionId, awnser, correctAwnser })
    } else {
      const awnserIndex = state.awnseredQuizzes[quizIndex].awnsers.findIndex(
        awnser => awnser.question_id === questionId
      )
      if (awnserIndex >= 0) {
        commit("updateAwnser", {
          quizIndex,
          awnserIndex,
          awnser,
          correctAwnser
        })
      } else {
        commit("addAwnser", { quizIndex, questionId, awnser, correctAwnser })
      }
    }
  }
}

const mutations = {
  addQuizAndAwnsers(state, { quizId, questionId, awnser, correctAwnser }) {
    state.awnseredQuizzes.push({
      quiz_id: quizId,
      awnsers: [
        {
          question_id: questionId,
          awnser: awnser,
          correctAwnser: correctAwnser
        }
      ]
    })
  },
  addAwnser(state, { quizIndex, questionId, awnser, correctAwnser }) {
    state.awnseredQuizzes[quizIndex].awnsers.push({
      question_id: questionId,
      awnser: awnser,
      correctAwnser: correctAwnser
    })
  },
  updateAwnser(state, { quizIndex, awnserIndex, awnser, correctAwnser }) {
    state.awnseredQuizzes[quizIndex].awnsers[awnserIndex].awnser = awnser
    state.awnseredQuizzes[quizIndex].awnsers[awnserIndex].correctAwnser = correctAwnser
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
