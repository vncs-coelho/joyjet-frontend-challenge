const state = {
  items: []
}

const getters = {
  getQuizzes: state => {
    return state.items
  },
  getQuizById: state => id => {
    return state.items[id]
  }
}

const actions = {
  saveQuiz({ commit, state }, quiz) {
    if (quiz.id) {
      commit("updateQuiz", quiz.id, quiz)
    } else {
      quiz.id = state.items.length
      commit("pushQuiz", quiz)
    }

    return quiz.id
  }
}

const mutations = {
  pushQuiz(state, quizData) {
    state.items.push(quizData)
  },
  updateQuiz(state, id, quizData) {
    state.items[id] = quizData
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
